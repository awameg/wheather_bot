import telebot
from decouple import config
from temp import temp, temp2, kutuzova, center
from wind import wind
from summary import pogoda
from garage import garage
import logging

logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

API_KEY = config('token')


def telegram_bot(token):
    bot = telebot.TeleBot(token)

    @bot.message_handler(commands=['start', 'help'])
    def send_welcome(message):
        bot.send_message(message.chat.id, "Привет, я бот Рыгун. Я знаю следующие комманды:\n\n"
                                          "/temp - текущая температура в районе ПГТ Емельяново\n"
                                          "/temp2 - текущая температура в районе ПГТ Емельяново (датчик 2)\n"
                                          "/wind - текущая скорость ветра в районе ПГТ Емельяново\n"
                                          "/kutuzova - текущая температура на улице Кутузова - г.Красноярск\n"
                                          "/center - текущая температура в центре - г.Красноярск\n"
                                          "/pogoda - погодная сводка - г.Красноярск")
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.row('/temp', '/temp2', '/wind')
        keyboard.row('/kutuzova', '/center', '/pogoda')
        bot.send_message(message.from_user.id, 'Выберите:', reply_markup=keyboard)

    @bot.message_handler(commands=['temp'])
    def send_temp(message):
        current_data = temp()
        if float(current_data) < -20:
            bot.send_message(message.chat.id, f'🥶️Текущая температура на улице:  {current_data} \u00b0С 🥶')
        else:
            bot.send_message(message.chat.id, f'🌡️Текущая температура на улице:  {current_data} \u00b0С')

    @bot.message_handler(commands=['temp2'])
    def send_temp2(message):
        current_data = temp2()
        if float(current_data) < -20:
            bot.send_message(message.chat.id, f'🥶️Текущая температура на улице:  {current_data} \u00b0С 🥶')
        else:
            bot.send_message(message.chat.id, f'🌡️Текущая температура на улице:  {current_data} \u00b0С')

    @bot.message_handler(commands=['kutuzova'])
    def send_kutuzova(message):
        current_data = kutuzova()
        bot.send_message(message.chat.id, f'🌡️Текущая температура на улице Кутузова:  {current_data} \u00b0С')

    @bot.message_handler(commands=['center'])
    def send_center(message):
        current_data = center()
        bot.send_message(message.chat.id, f'🌡️Текущая температура в центре города:  {current_data} \u00b0С')

    @bot.message_handler(commands=['garage'])
    def send_center(message):
        current_data = garage()
        bot.send_message(message.chat.id, f'🚙Температура в гараже:  {current_data} \u00b0С')

    @bot.message_handler(commands=['wind'])
    def send_wind(message):
        current_data = wind()
        bot.send_message(message.chat.id, f'💨Скорость ветра на улице:  {current_data} м/с')

    @bot.message_handler(commands=['pogoda'])
    def send_pogoda(message):
        current_data = pogoda()
        bot.send_message(message.chat.id, f'🌡️Текущая температура: {current_data[0]} \u00b0С\n'
                                          f'💦Влажность: {current_data[1]}%\n'
                                          f'🧱Атмосферное давление: {current_data[2]} mmHg\n'
                                          f'💨Скорость ветра на улице:  {current_data[3]} м/с')

    @bot.message_handler(content_types=['text'])
    def send_ops(message):
        if 'ой' in message.text.lower():
            with open('img/img.png', 'rb') as img_file:
                bot.send_photo(message.chat.id, img_file)

    bot.infinity_polling(timeout=10, long_polling_timeout=60)


if __name__ == '__main__':
    telegram_bot(API_KEY)
