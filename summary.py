import json
import requests
import re


# Скрипт парсит показания двух датчиков. На выходе список [температура, влажность, давление, скорость ветра]

# Погодная станция

def pogoda():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=78"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')

    current_date = re.findall(r'-?[0-9]..[0-9]*', data_res)

    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=84"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_wind = data.get('result')[0].get('Data')
    current_wind = re.findall(r'[0-9].', data_wind)[0].strip()
    current_date.append(current_wind)

    return current_date


# print(pogoda())
