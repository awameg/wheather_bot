import json
import requests
import re


# Скрипт парсит показания погодной станции. На выходе список [температура, влажность, давление]

# Погодная станция
def temp():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=78"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')

    current_date = re.findall(r'-?[0-9]..[0-9]*', data_res)
    return current_date[0]


def temp2():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=59"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')

    current_date = re.findall(r'-?[0-9]..[0-9]*', data_res)
    return current_date[0]


def kutuzova():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=125"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')

    current_date = re.findall(r'-?[0-9]..[0-9]*', data_res)
    return current_date[0]


def center():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=74"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')

    current_date = re.findall(r'-?[0-9]..[0-9]*', data_res)
    return current_date[0]


print(temp2())