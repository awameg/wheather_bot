import json
import requests
import re


# Скрипт парсит показания погодной станции. На выходе список [скорость ветра]

# Погодная станция
def wind():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=84"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')
    current_date = re.findall(r'[0-9].', data_res)[0].strip()
    return current_date

# print(wind())
