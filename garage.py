import json
import requests
import re


def garage():
    url = "http://192.168.1.4:8090/json.htm?type=devices&rid=91"

    response = requests.get(url=url)
    data = json.loads(response.text)
    data_res = data.get('result')[0].get('Data')
    current_date = re.findall(r'[0-9].[0-9]*', data_res)
    return current_date[0]


# print(garage())
