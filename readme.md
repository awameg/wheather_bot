Установка:
Создайте нового бота у @BotFather.

Создайте файл .env и поместить в него токин, для доступа к боту и rapidapi
token=
rapidapi=

В директории с файлами бота необходимо создать виртуальное окружение:
python3 -m venv venvBOT

И активировать его:
source venvBOT/bin/activate

Установите необходимые библеотеки:
pip install requirements.txt
или
python -m pip install -r requirements.txt

Затем запустите бота коммандой:
python3 main.py
